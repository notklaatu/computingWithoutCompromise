# Computing Without Compromise

A non-fiction book I published on Smashwords. I wrote it because I was
not finding enough books that were about Linux and open source without
being manuals on its use. This book is light reading about being a
computer nerd in the age of open source technology. It covers a lot of
ground but can be summed up as my musings on being a member of the
open source community, and why that's fun.

## Back cover text:

This isn't a book about how to use Linux, it's not a book explaining
what Linux is, it's not even an academic analysis of the
socio-economic impact of Big Data management.

This is a book about being an open source and Linux geek. It's about
what makes it fun and oh-so-supremely satisfying to be a computer nerd
in the age of open source technology. It's meant to be a fun book;
it's the conversation you'd have with your computer pals at a coffee
shoppe, or at the local hobby shoppe. It's the rambling thoughts of
someone who really likes to think about open source.

And Klaatu does think a lot about open source. A Linux consultant by
day, he has worked with clients to help them implement and manage open
source in their workflows, and with indie artists to help them
discover exciting new, non-corporate options in their creative
process. He records his incidental thoughts for podcasts such as
Hacker Public Radio and GNU World Order.

## Building

There's really no reason to build this book yourself; it's free on
Smashwords. You can download the epub for free; I don't even think it
requires registration with Smashwords to get it.

If you really want to build it, though, the easiest way to do that is
with [pandoc](http://pandoc.org). The command to run is included in
this git repository. It should pretty much just work.

Here it is:

    pandoc -f markdown -t epub3 book.md \
    colophon.md -N --epub-stylesheet=style.css \
    --epub-metadata=metadata.xml \
    --epub-cover-image=./images/cover-front.svg \
    --epub-embed-font=kabel.ttf \
    --epub-embed-font=Nouveau_IBM.ttf \
    -o klaatu_computingWithoutCompromise.epub

To be clear: you don't need to do that to read this book. It's available for free from Smashwords.

Enjoy!